/* Only load when detecting large screen sizes.
import breakpoint from '/themes/custom/ethoca/js/breakpoints.js'; */

breakpoint.refreshValue();

if (breakpoint.value === 'large') {
  // console.log(breakpoint.value);

  (function ($) {

  'use strict';

  // Y axis scroll speed
  var velocity = 0.4;

  function update(){
      var pos = $(window).scrollTop();
      $('.paragraph-type-hero').each(function() {
          var $element = $(this);
          // subtract some from the height b/c of the padding
          //var height = $element.height();
          var height = -550;
          $(this).css('backgroundPosition', '50% ' + Math.round((height - pos) * velocity) + 'px');
          //console.log(height);
          //console.log(pos);
      });
      $('.careers-hero').each(function() {
          var $element = $(this);
          // subtract some from the height b/c of the padding
          // var height = $element.height();
          var height = -550;
          $(this).css('backgroundPosition', '50% ' + Math.round((height - pos) * velocity) + 'px');
          //console.log(height);
          //console.log(pos);
      });
      $('.news-hero-press-releases').each(function() {
          var $element = $(this);
          // subtract some from the height b/c of the padding
          // var height = $element.height();
          var height = -550;
          $(this).css('backgroundPosition', '50% ' + Math.round((height - pos) * velocity) + 'px');
          //console.log(height);
          //console.log(pos);
      });
      $('.news-hero-in-the-media').each(function() {
          var $element = $(this);
          // subtract some from the height b/c of the padding
          // var height = $element.height();
          var height = -550;
          $(this).css('backgroundPosition', '50% ' + Math.round((height - pos) * velocity) + 'px');
          //console.log(height);
          //console.log(pos);
      });
      $('.events-hero').each(function() {
          var $element = $(this);
          // subtract some from the height b/c of the padding
          // var height = $element.height();
          var height = -550;
          $(this).css('backgroundPosition', '50% ' + Math.round((height - pos) * velocity) + 'px');
          //console.log(height);
          //console.log(pos);
      });
      $('.news-hero-blog').each(function() {
          var $element = $(this);
          // subtract some from the height b/c of the padding
          // var height = $element.height();
          var height = -550;
          $(this).css('backgroundPosition', '50% ' + Math.round((height - pos) * velocity) + 'px');
          //console.log(height);
          //console.log(pos);
      });
      $('.leadership-hero').each(function () {
          var $element = $(this);
          // subtract some from the height b/c of the padding
          // var height = $element.height();
          var height = -550;
          $(this).css('backgroundPosition', '50% ' + Math.round((height - pos) * velocity) + 'px');
          //console.log(height);
          //console.log(pos);
      });
      /*
      $('.paragraph--type--hero-special').each(function() {
          var $element = $(this);
          // subtract some from the height b/c of the padding
          // var height = $element.height();
          var height = -550;
          $(this).css('backgroundPosition', '50% ' + Math.round((height - pos) * velocity) + 'px');
          console.log(height);
          console.log(pos);
      });
      */
  };

  $(window).bind('scroll', update);

  })(jQuery);

}
