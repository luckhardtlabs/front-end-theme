/**
 * @file
 * Detects breakpoints from CSS.
 */

// Query the content property on the ::before pseudo element
const breakpoint = {};

breakpoint.refreshValue = function refreshValue() {
  this.value = window
    .getComputedStyle(document.querySelector('body'), ':before')
    .getPropertyValue('content')
    .replace(/"/g, '');
};

/**
 * Capture the content property value on the::before pseudo element with the
 * page finishes loading.
 *
 * @returns {string} breakpoint value.
 */
function detectBrowserSize() {
  breakpoint.refreshValue();
  /*
  if (breakpoint.value === 'large') {
    console.log(`detectBrowserSize: ${window.innerWidth}`);
  }
  */
}

// Detect the browser viewport size when the page loads.
window.addEventListener('load', detectBrowserSize);

/**
 * Capture the content property value on the::before pseudo element when the
 * browser resizes.
 *
 * @returns {string} breakpoint value.
 */
/*
window.addEventListener("resize", onResizeFunction);
function onResizeFunction() {
  breakpoint.refreshValue();
  console.log('onResizeFunction: ' + window.innerWidth);
}
*/
