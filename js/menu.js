/* Only load when detecting large screen sizes.
import breakpoint from '/themes/custom/ethoca/js/breakpoints.js'; */

breakpoint.refreshValue();

if (breakpoint.value === 'large') {

  let navItem = document.querySelectorAll('.ultimenu__flyout');
  const FLYOUT_POSITION_OFFSET = 109;

  navItem.forEach(function(navItem, index) {
    index = index + 1;

    // let navHeading = document.querySelector(".uitem--" + index + ' a').innerHTML;
    // let navHeadingLI = document.querySelector(".uitem--" + index);
    // let navHeadingLISection = document.querySelector(".uitem--" + index + " .ultimenu__flyout");

    // Set position of all flyouts to be fixed, top = 109px, left = 0px
    navItem.style.left = "0px";
    navItem.style.position = "fixed";
    navItem.style.top = FLYOUT_POSITION_OFFSET + 'px';

    // console.log(navHeading + ' /// navItem.style.left = ' + navItem.style.left + ' /// navItem.style.position = ' + navItem.style.position + ' /// navItem.style.top = ' + navItem.style.top);

    // Initialize state.
    let scrollPosition = 0;

    // Detect current scroll position of viewport (with scroll lisenter event).
    window.addEventListener('scroll', function () {
      // If scroll increases beyond 0, move all flyouts up with equivalent page scroll
      if (pageYOffset > scrollPosition) {
        // Subtract pageYOffset from .ultimenu__flyout top position.
        navItem.style.top = FLYOUT_POSITION_OFFSET - pageYOffset + 'px';
      } else {
        navItem.style.top = FLYOUT_POSITION_OFFSET + 'px';
      }
    });
  });
}
