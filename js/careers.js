/* Only load when detecting large screen sizes.
import breakpoint from '/themes/custom/ethoca/js/breakpoints.js'; */

breakpoint.refreshValue();

if (breakpoint.value === 'large') {
  // console.log(breakpoint.value);

  // Add ID attribute to office location unordered list element.
  const el = document.querySelectorAll('.careers-location-nav__list');

  if (el.length) {
    el[0].id = 'careers-location-nav__list';
  }

  // Prepend label to dynamically generated list of locations.
  document.getElementById('careers-location-nav__list').
    insertAdjacentHTML(
      'beforebegin',
      '<div class="filter-by-location">Filter by location:</div>'
    );
  // Prepend option 'All' to dynamically generated list of locations.
  document.getElementById('careers-location-nav__list').
    insertAdjacentHTML(
      'afterbegin',
      '<li class="careers-location-nav__item"><a href="#all">All</a></li>'
    );

  // Add click event listener on all office location unordered list items.
  document.getElementById('careers-location-nav__list').
    addEventListener('click', (el) => {
      if (el.target && el.target.matches('li.careers-location-nav__item a')) {
        const locationHash = el.target.hash,
          trClassAll = document.
            getElementsByClassName('career-location'),
          trClassAustin = document.
            getElementsByClassName('career-location austin'),
          trClassDublin = document.
            getElementsByClassName('career-location dublin'),
          trClassLondon = document.
            getElementsByClassName('career-location london'),
          trClassMelbourne = document.
            getElementsByClassName('career-location melbourne'),
          trClassParis = document.
            getElementsByClassName('career-location paris'),
          trClassPhoenix = document.
            getElementsByClassName('career-location phoenix'),
          trClassRemote = document.
            getElementsByClassName('career-location remote'),
          trClassToronto = document.
            getElementsByClassName('career-location toronto');

        let i = 0;

        switch (locationHash) {
        case '#toronto':
          for (i = 0; i < trClassAll.length; i += 1) {
            trClassAll[i].classList.remove('is-visible');
            trClassAll[i].classList.remove('is-hidden');
          }
          for (i = 0; i < trClassAll.length; i += 1) {
            trClassAll[i].classList.add('is-hidden');
          }
          for (i = 0; i < trClassToronto.length; i += 1) {
            trClassToronto[i].classList.add('is-visible');
            trClassToronto[i].classList.remove('is-hidden');
          }
          break;
        case '#austin':
          for (i = 0; i < trClassAll.length; i += 1) {
            trClassAll[i].classList.remove('is-visible');
            trClassAll[i].classList.remove('is-hidden');
          }
          for (i = 0; i < trClassAll.length; i += 1) {
            trClassAll[i].classList.add('is-hidden');
          }
          for (i = 0; i < trClassAustin.length; i += 1) {
            trClassAustin[i].classList.add('is-visible');
            trClassAustin[i].classList.remove('is-hidden');
          }
          break;
        case '#phoenix':
          for (i = 0; i < trClassAll.length; i += 1) {
            trClassAll[i].classList.remove('is-visible');
            trClassAll[i].classList.remove('is-hidden');
          }
          for (i = 0; i < trClassAll.length; i += 1) {
            trClassAll[i].classList.add('is-hidden');
          }
          for (i = 0; i < trClassPhoenix.length; i += 1) {
            trClassPhoenix[i].classList.add('is-visible');
            trClassPhoenix[i].classList.remove('is-hidden');
          }
          break;
        case '#london':
          for (i = 0; i < trClassAll.length; i += 1) {
            trClassAll[i].classList.remove('is-visible');
            trClassAll[i].classList.remove('is-hidden');
          }
          for (i = 0; i < trClassAll.length; i += 1) {
            trClassAll[i].classList.add('is-hidden');
          }
          for (i = 0; i < trClassLondon.length; i += 1) {
            trClassLondon[i].classList.add('is-visible');
            trClassLondon[i].classList.remove('is-hidden');
          }
          break;
        case '#paris':
          for (i = 0; i < trClassAll.length; i += 1) {
            trClassAll[i].classList.remove('is-visible');
            trClassAll[i].classList.remove('is-hidden');
          }
          for (i = 0; i < trClassAll.length; i += 1) {
            trClassAll[i].classList.add('is-hidden');
          }
          for (i = 0; i < trClassParis.length; i += 1) {
            trClassParis[i].classList.add('is-visible');
            trClassParis[i].classList.remove('is-hidden');
          }
          break;
        case '#dublin':
          for (i = 0; i < trClassAll.length; i += 1) {
            trClassAll[i].classList.remove('is-visible');
            trClassAll[i].classList.remove('is-hidden');
          }
          for (i = 0; i < trClassAll.length; i += 1) {
            trClassAll[i].classList.add('is-hidden');
          }
          for (i = 0; i < trClassDublin.length; i += 1) {
            trClassDublin[i].classList.add('is-visible');
            trClassDublin[i].classList.remove('is-hidden');
          }
          break;
        case '#melbourne':
          for (i = 0; i < trClassAll.length; i += 1) {
            trClassAll[i].classList.remove('is-visible');
            trClassAll[i].classList.remove('is-hidden');
          }
          for (i = 0; i < trClassAll.length; i += 1) {
            trClassAll[i].classList.add('is-hidden');
          }
          for (i = 0; i < trClassMelbourne.length; i += 1) {
            trClassMelbourne[i].classList.add('is-visible');
            trClassMelbourne[i].classList.remove('is-hidden');
          }
          break;
        case '#remote':
          for (i = 0; i < trClassAll.length; i += 1) {
            trClassAll[i].classList.remove('is-visible');
            trClassAll[i].classList.remove('is-hidden');
          }
          for (i = 0; i < trClassAll.length; i += 1) {
            trClassAll[i].classList.add('is-hidden');
          }
          for (i = 0; i < trClassRemote.length; i += 1) {
            trClassRemote[i].classList.add('is-visible');
            trClassRemote[i].classList.remove('is-hidden');
          }
          break;
        default:
          for (i = 0; i < trClassAll.length; i += 1) {
            trClassAll[i].classList.add('is-visible');
            trClassAll[i].classList.remove('is-hidden');
          }
        }
      }
    });
}

