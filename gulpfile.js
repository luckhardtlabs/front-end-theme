/**
 * @file
 * The gulp task runner, for automated front-end development.
 */

/**
 * Include gulp and various tasks.
 */
var gulp = require('gulp');
var browserSync = require('browser-sync');
var notify = require('gulp-notify');
var postcss = require('gulp-postcss');
var prefix = require('gulp-autoprefixer');
var sass = require('gulp-sass');
var shell = require('gulp-shell');
var sourcemaps = require('gulp-sourcemaps');

/**
 * General error notifications and logging.
 */
function handleErrors(error) {
  notify().write(error);
  console.log(error);
}

var sassOptions = {
  errLogToConsole: true,
  outputStyle: 'compressed'
};

/**
 * @task browser-sync
 * Launch the localhost server.
 */
gulp.task('browser-sync', ['sass'], function () {
  browserSync.init({
    // Modify the port and proxy as needed.
    port: "3846",
    proxy: "ethoca.test",
      socket: {
      // For local development only use the default Browsersync local URL.
      domain: 'http://localhost:3846'
      // For external development (e.g on a mobile or tablet) use an external URL.
      // You will need to update this to whatever Browsersync tells you is the
      // external URL when you run Gulp.
      // domain: 'http://10.148.148.143:3000'
    }
  });
});

/**
 * @task sass
 * Compile files from scss.
 */
gulp.task('sass', function () {
  return gulp

  // Identify the source .scss file.
  .src('sass/main.scss')

  // Initialize sourcemaps to the stream.
  .pipe(sourcemaps.init())

  // Display errors.
  .pipe(sass(sassOptions).on('error', handleErrors))

  // Pass the file to "gulp-autoprefixer".
  // Reference: http://browserl.ist/?q=%3E+1%25
  .pipe(prefix(['last 2 versions', '> 1%'], { cascade: true }))

  // Write sourcemaps to the stream.
  .pipe(sourcemaps.write('.'))

  // Output a main.css file to the css directory.
  .pipe(gulp.dest('css'))

  // Reload the stream.
  .pipe(browserSync.reload({stream:true}))
});

/**
 * @task clearcache
 * Clear all of Drupal's caches.
 */
gulp.task('clearcache', function () {
  return shell.task(['drupal cr all']);
});

/**
 * @task reload
 * Refresh the page after clearing caches.
 */
gulp.task('reload', ['clearcache'], function () {
  browserSync.reload();
});

/**
 * @task watch
 * Watch scss files for changes and perform recompilations.
 * Clear caches when Drupal related files are changed.
 */
gulp.task('watch', function () {
  gulp.watch(['sass/*.scss', 'sass/**/*.scss'], ['sass']);
  gulp.watch('**/*.{html,css,php,twig}', ['reload']);
});

/**
 * Default task, running just `gulp` will compile Sass files, launch Browsersync & watch files.
 */
gulp.task('default', ['browser-sync', 'watch']);
